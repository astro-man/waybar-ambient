# waybar-ambient

## Name
AMBIENT WEATHER PYTHON SCRIPT FOR WAYBAR

## Description
waybar-ambient is a python script that can be used in a waybar custom module to pull weather data from your personal Ambient weather device and display in waybar. It does this by connecting to the [ambientweather.net](https://ambientweather.net) API and pulling the data you upload to them from your device.

## Visuals
![Waybar Module Example](images/example.png "waybar example")

## Installation
1. Download the waybar-ambient.py script and place it in a location of your choice. Creating a 'scripts' directory within your waybar directory is recommended.
2. Login to your account on [ambientweather.net/account](https://ambientweather.net/account).
3. Scroll down to the bottom of the page and find API Keys and click "Create API Key".
4. Under the API Keys section, find, in small print, "Developers: An Application Key is also required for each application that you develop. Click here to create one." Click the link to create your application key.
5. Edit the waybar-ambient.py script and add your api and developer keys to the https request...
```
weather = requests.get("https://api.ambientweather.net/v1/devices/?apiKey=<your-api-key>&applicationKey=<your-application-key>&limit=1", timeout=10).json() # 10 seconds
```
NOTE: You only ever need one application key, but you will need to create a seperate api key for each PC you run the script on. For more information about Ambient Weather's API see [ambientweather API Docs](https://ambientweather.docs.apiary.io/#reference/0/device-data).

* Rate Limiting:
    API requests are capped at 1 request/second for each user's apiKey and 3 requests/second per applicationKey. When this limit is exceeded, the API will return a 429 response code.


## Usage

1. Create a custom module within your waybar config...
```
"custom-weather": {
    "tooltip": true,
    "format": {},
    "restart-interval": 300,
    "exec": ".config/waybar/scripts/waybar-ambient.py"
    "return-type": "json"
},
```
2. Add the newly created module to one of, "modules-left", "modules-center" or "modules-right" of the waybar config.
```
"modules-left": ["clock","custom/weather","hyprland/workspaces"],
```

3. Create a style for your module in the waybar style.css file...
...
```
#custom-weather
...
#custom-weather {
<your style selections here>
}
```
4. Reload waybar

## Authors and acknowledgment
Inspired by wttrbar. [waybar-wttr](https://gist.github.com/bjesus/f8db49e1434433f78e5200dc403d58a3)

## License
This script is not licensed.

