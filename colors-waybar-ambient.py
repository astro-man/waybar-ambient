#!/usr/bin/env python
# NOTE: This pyhton script has been formatted to work with pywal. Escape braces have been used around python variables.
import json
import requests
import os
import sys
from datetime import datetime, timezone
script_dir = os.path.dirname(__file__)
data = {{}}
try:
    weather = requests.get("https://api.ambientweather.net/v1/devices/?apiKey=bf7fe3c97adb440ca586f434407c8cc9eda2da831a894a72bdf925f7a56abd60&applicationKey=3a9ec97c8eaf40818244b8f38f9cab3b518d977c438444ba8575a95fd7529aa1&limit=1", timeout=10).json() # 10 seconds
except requests.exceptions.Timeout: # Handle if we can't connect to ambient.
    with open(os.path.join(script_dir,"static-weather-ambient.json"), 'r') as file:
     jsonStr = file.read()
     weather = json.loads(jsonStr) # load a static file so we don't lose weather in waybar.
except requests.exceptions.ConnectionError: # Handle if we can't connect to internet.
    with open(os.path.join(script_dir,"static-weather-ambient.json"), 'r') as file:
     jsonStr = file.read()
     weather = json.loads(jsonStr) # load a static file so we don't lose weather in waybar.

# Detect over user limit (1s requests)
if sys.getsizeof(weather) != 184:
# Save a copy
 with open(os.path.join(script_dir,"static-weather-ambient.json"), 'w', encoding='utf-8') as f:
  json.dump(weather, f, ensure_ascii=False, indent=4)
  print(sys.getsizeof(weather))
else:
   print(sys.getsizeof(weather))
   with open(os.path.join(script_dir,"static-weather-ambient.json"), 'r') as file:
    jsonStr = file.read()
    weather = json.loads(jsonStr) # load a static file so we don't lose weather in waybar.

# Formatting some data
fTemp = float(weather[0]['lastData']['tempf'])
wCode = float(weather[0]['lastData']['solarradiation'])
elev = round(float(weather[0]['info']['coords']['elevation']),2)
hRain = round((float(weather[0]['lastData']['hourlyrainin'])),2)
dRain = round((float(weather[0]['lastData']['dailyrainin'])),2)
eRain = round((float(weather[0]['lastData']['eventrainin'])),2)
wRain = round((float(weather[0]['lastData']['weeklyrainin'])),2)
mRain = round((float(weather[0]['lastData']['monthlyrainin'])),2)
tRain = round((float(weather[0]['lastData']['totalrainin'])),2)
lon = round((float(weather[0]['info']['coords']['coords']['lon'])),2)
lat = round((float(weather[0]['info']['coords']['coords']['lat'])),2)


# Time
def zuluToLocal(zTime):
    local = datetime.fromisoformat(zTime).replace(tzinfo=timezone.utc).astimezone()
    local_formatted = local.strftime('%Y-%m-%d %I:%M %p')
    return local_formatted

# Condition icons
def srvswitch(srv):
    if srv >= 700.0:
        return "☀️"
    elif srv >= 500.0:
        return "⛅"
    elif srv >= 300.0:
        return "⛅"
    elif srv > 0.0:
        return "☁️"
    elif srv == 0.0:
        return "🌜"
    else:
        return "⛔"

# Wind Direction, degree to direction.
def wdswitch(dir):
    if dir >= 350 or dir <= 10:
        return "North"
    elif dir >= 328:
        return "NNW"
    elif dir >= 304:
        return "NW"
    elif dir >= 282:
        return "WNW"
    elif dir >= 259:
        return "W"
    elif dir >= 236:
        return "WSW"
    elif dir >= 214:
        return "SW"
    elif dir >= 192:
        return "SSW"
    elif dir >= 169:
        return "S"
    elif dir >= 146:
        return "SSE"
    elif dir >= 124:
        return "SE"
    elif dir >= 101:
        return "ESE"
    elif dir >= 79:
        return "E"
    elif dir >= 56:
        return "ENE"
    elif dir >= 34:
        return "NE"
    elif dir >= 11:
        return "NNE"
    else:
        return "⛔"
# Battery Health
def batswitch(batout):
    if batout == 1:
        return "OK"
    elif batout == 0:
        return "Low"
    else:
        return "⛔"

# Change temp color based on temp
def tcswitch(fTemp):
    if fTemp >= 100:
        return "#E60000"
    elif fTemp >= 90:
        return "#FF5500"
    elif fTemp >= 80:
        return "#FFAA00"
    elif fTemp >= 70:
        return "#D4FF00"
    elif fTemp >= 60:
        return "#00FF00"
    elif fTemp >= 50:
        return "#00FFD5"
    elif fTemp >= 40:
        return "#00FFFF"
    elif fTemp >= 30:
        return "#0080FF"
    elif fTemp >= 20:
        return "#002AFF"
    elif fTemp >= 10:
        return "#5500FF"
    elif fTemp >= 0:
        return "#AA00FF"
    elif fTemp >= -10:
        return "#FF00FF"
    else:
        return "#99B3FF"

if hRain > 0.00:
    data['text'] = f"☔ <span color='{{tcswitch(fTemp)}}'>{{fTemp}}°F</span> "
else:
    data['text'] = f"{{srvswitch(wCode)}} <span color='{{tcswitch(fTemp)}}'>{{fTemp}}°F</span> "

data['tooltip'] = f"       <span color='{foreground}'>Station:</span><span color='{color11}' size='10pt'> {{weather[0]['info']['name']}}</span>\n"
data['tooltip'] += f"      <span color='{foreground}'>Location:</span><span color='{color11}' size='10pt'> {{weather[0]['info']['coords']['location']}}</span>\n"
data['tooltip'] += f"     <span color='{foreground}'>Elevation:</span><span color='{color11}' size='10pt'> {{elev}}</span>\n"
data['tooltip'] += f"   <span color='{foreground}'>Coordinates:</span><span color='{color11}' size='10pt'> Lon:{{lon}}, Lat:{{lat}}</span>\n"
data['tooltip'] += f"          <span color='{foreground}'>Date:</span><span color='{color11}' size='10pt'> {{zuluToLocal(weather[0]['lastData']['date'])}}</span>\n"
data['tooltip'] += f"\n"
data['tooltip'] += f"<span color='{foreground}' size='10pt'>    <u>Current Conditions</u></span>"
data['tooltip'] += f"\n"
data['tooltip'] += f"    <span color='{foreground}'>Feels Like:</span><span color='{color11}' size='10pt'> {{weather[0]['lastData']['feelsLike']}}°F</span>\n"
data['tooltip'] += f"      <span color='{foreground}'>Humidity:</span><span color='{color11}' size='10pt'> {{weather[0]['lastData']['humidity']}}%</span>\n"
data['tooltip'] += f"          <span color='{foreground}'>Wind:</span><span color='{color11}' size='10pt'> {{wdswitch(weather[0]['lastData']['winddir'])}}({{weather[0]['lastData']['winddir']}}°) Speed:{{weather[0]['lastData']['windspeedmph']}}mph Gusting:{{weather[0]['lastData']['windgustmph']}}mph Max:{{weather[0]['lastData']['maxdailygust']}}mph</span>\n"
data['tooltip'] += f"          <span color='{foreground}'>Rain:</span><span color='{color11}' size='10pt'> Rate:{{hRain}}in/hr Daily:{{dRain}}in Event:{{eRain}}in:</span>\n"
data['tooltip'] += f"     <span color='{foreground}'>Last Rain:</span><span color='{color11}' size='10pt'> {{zuluToLocal(weather[0]['lastData']['lastRain'])}}</span>\n"
data['tooltip'] += f"   <span color='{foreground}'>Rain Totals:</span><span color='{color11}' size='10pt'> Weekly:{{wRain}}in Monthly:{{mRain}}in Total:{{tRain}}in</span>\n"
data['tooltip'] += f"<span color='{foreground}'>Battery Health:</span><span color='{color11}' size='10pt'> {{batswitch(weather[0]['lastData']['battout'])}}</span>\n"

print(json.dumps(data))
